//
//  ViewController.swift
//  HWTR2
//
//  Created by ÐžÐ»ÐµÐºÑÐ°Ð½Ð´Ñ€ ÐšÐ°Ñ…ÐµÑ€ÑÐºÐ¸Ð¹ on 30.08.2018.
//  Copyright Â© 2018 ÐžÐ»ÐµÐºÑÐ°Ð½Ð´Ñ€ ÐšÐ°Ñ…ÐµÑ€ÑÐºÐ¸Ð¹. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var secondTextField: UITextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
		buildPyramid(number: 3)
		
	}
	
	func buildPyramid(number: Int) {
		let step = 40
		var x = 144
		var y = 0
		var count = 0
		for _ in 0..<number {
			y += step
			count += 1
			for _ in 0..<count {
				buildCube(x: x, y: y)
				x += step
			}
			x = x - 20 - (40 * count)
		}
	}
	
	func buildLedder(number: Int) {
		let step = 40
		var x = 40
		var y = 0
		var count = 0
		for _ in 0..<number {
			y += step
			count += 1
			for _ in 0..<count {
				buildCube(x: x, y: y)
				x += step
			}
			x = 40
		}
	}
	
	func buildLine(number: Int) {
		let y = 40
		var x = 0
		for _ in 0..<number {
			x += 40
			buildCube(x: x, y: y)
		}
	}
	
	var tag = 0
	
	func buildCube(x: Int, y: Int) {
		let size = 32
		let rect = CGRect.init(x: x, y: y, width: size, height: size)
		let box = UIButton.init(frame: rect)
		tag += 1
		box.tag = tag
		box.setTitle(tag.description, for: .normal)
		box.backgroundColor = UIColor.gray
		box.addTarget(self, action: #selector(buttonPressed(sender:)), for: UIControl.Event.touchUpInside)
		view.addSubview(box)
	}
	
	@objc func buttonPressed(sender: UIButton) {
		print(sender.tag)
	}
	
}

extension ViewController : UITextFieldDelegate {
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField.tag == 0 {
			secondTextField.becomeFirstResponder()
		}
		else {
			textField.resignFirstResponder()
		}
		return false
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		view.endEditing(true)
	}
}
